# configfile

Store application settings in a (mostly) standardized place.

This is inspired by Qt QSettings and stores the config file in:

```~/.config/<company>/<application>/<file>```

This uses the json format as it is convenient for python data types.

This allows to read a persistent configuration and specify a default value.

If the parameter get(..., 'immediate = True') or store() are called, the config file is written.
This is convenient to bootstrap an application when no config file exists yet.

## Install

- install this folder in your PYTHONPATH
- use with ```from configfile import ConfigFile```

## Example

```python
from configfile import ConfigFile

cf = ConfigFile("mrrabbit", "configfile")
my_value = cf.get(["path", "to", "object"], "default_value")
```
