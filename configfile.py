#!/usr/bin/env python

# configfile
#
# Copyright (C) 2020 MrRabbit
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses
#
# contact: dev at mrabb dot it


"""
    @package
    ~/.config/<company>/<application>/<file> JSON config files handling
"""

from pathlib import Path
import json

class ConfigFile:

    def __init__(self, company, application, file_name="config.json"):
        self.file_path = "%s/.config/%s/%s/%s" % (Path.home(), company, application, file_name)
        self._file = Path(self.file_path)
        if self._file.is_file():
            with self._file.open() as json_file:
                try:
                    self.config = json.load(json_file)
                except json.decoder.JSONDecodeError:  # empty file
                     self.config = {}
        else:
            self._file.parent.mkdir(parents=True, exist_ok=True)
            self._file.touch(mode=0o600)
            self.config = {}

    def values(self):
        return self.config

    def parse_kwargs(self, path, **kwargs):
        separator = kwargs.get('separator', '/')
        default = kwargs.get('default', None)
        immediate = kwargs.get('immediate', False)
        if type(path) == str:
            path = path.split(separator)
        return path, default, immediate

    def getRoot(self):
        return self.config

    def get(self, path, **kwargs):
        path, default, immediate = self.parse_kwargs(path, **kwargs)
        c = self.config
        for i, p in enumerate(path):
            is_last = i == len(path) - 1
            if p in c:
                if is_last:
                    return c[p]
                c = c[p]
            elif default:
                self.setField(path, default, **kwargs)
                return default
        return None

    def setRoot(self, value, immediate = False):
        self.config = value
        if immediate:
            self.store()

    def setField(self, path, value, **kwargs):
        path, _, immediate = self.parse_kwargs(path, **kwargs)
        c = self.config
        for i, p in enumerate(path):
            is_last = (i == len(path) - 1)
            if p in c:
                if is_last:
                    c[p] = value
                    if immediate:
                        self.store()
                    return
            else:
                if is_last:
                    c[p] = value
                    if immediate:
                        self.store()
                    return
                else:
                    c[p] = {}
            c = c[p]

    def store(self):
         with self._file.open(mode='w') as json_file:
            json.dump(self.config, json_file, sort_keys=True, indent=4)

    def clear(self):
        self._file.unlink(missing_ok=True)
        self._file.touch(mode=0o600)
        self.config = {}

if __name__ == '__main__':

    import unittest

    class TestConfigFile(unittest.TestCase):

        def test_all(self):
            cf = ConfigFile("mrrabbit", "configfile")
            cf.clear()
            self.assertEqual(cf.get(["indent", "space"]), None)
            self.assertEqual(cf.get("indent/space", default=4), 4)
            self.assertEqual(cf.get(["indent", "space"]), 4)
            cf.setField("indent/use tab", False)
            self.assertEqual(cf.get(["indent", "use tab"]), False)
            cf.store()

    unittest.main()
